package com.project.springconsoleapp.reader;

import com.project.springconsoleapp.analyzer.DataItem;

public interface DataReader {
    DataItem read();
}
