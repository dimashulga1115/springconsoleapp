package com.project.springconsoleapp.reader;

import com.project.springconsoleapp.analyzer.DataItem;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class FileReader implements DataReader {

    private DataItem dataItem;

    public FileReader(String filepath) throws IOException {
        Path path = Paths.get(filepath);
        List<String> readLines = Files.readAllLines(path);
        StringBuffer stringBuffer = new StringBuffer();
        readLines.stream().forEach(s -> stringBuffer.append(s).append("\n"));
        dataItem = new DataItem(stringBuffer.toString());
    }

    @Override
    public DataItem read() {
        return dataItem;
    }
}
