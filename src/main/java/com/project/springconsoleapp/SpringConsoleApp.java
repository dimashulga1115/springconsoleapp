package com.project.springconsoleapp;

import com.project.springconsoleapp.analyzer.DataAnalyzer;
import com.project.springconsoleapp.config.ModuleConfigurationImpl;
import com.project.springconsoleapp.reader.FileReader;
import com.project.springconsoleapp.writer.sql.SqlWriter;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringConsoleApp implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(SpringConsoleApp.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        ModuleConfigurationImpl config = ModuleConfigurationImpl.getBuilder()
                .setDataReader(new FileReader("src/main/resources/files/file1.txt"))
                .setDataAnalyzer(new DataAnalyzer())
                .setDataWriter(new SqlWriter())
                .build();
        Module module = new Module(config);
        module.run();
    }
}
