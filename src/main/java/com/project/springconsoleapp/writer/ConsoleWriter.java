package com.project.springconsoleapp.writer;

import com.project.springconsoleapp.analyzer.DataReport;

public class ConsoleWriter implements DataWriter{

    public ConsoleWriter() {}

    @Override
    public void write(DataReport dataReport) {
        dataReport.getEntries().forEach(dataReportEntry -> System.out.println(dataReportEntry.toString()));
    }
}
