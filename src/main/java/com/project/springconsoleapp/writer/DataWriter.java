package com.project.springconsoleapp.writer;

import com.project.springconsoleapp.analyzer.DataReport;

public interface DataWriter {
    void write(DataReport dataReport);
}
