package com.project.springconsoleapp.writer.sql;

import com.project.springconsoleapp.analyzer.DataReport;
import com.project.springconsoleapp.writer.DataWriter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class SqlWriter implements DataWriter {

    JdbcTemplate jdbcTemplate;

    public SqlWriter() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/test?useSSL=false");
        dataSource.setUsername("testuser");
        dataSource.setPassword("testpass");

        jdbcTemplate = new JdbcTemplate(dataSource);
        System.out.println("Jdbc object: " + jdbcTemplate);
        jdbcTemplate.execute("DROP TABLE IF EXISTS reports;");
        jdbcTemplate.execute("CREATE TABLE reports (id SERIAL, longestName VARCHAR(255), shortestName VARCHAR(255), lineLength INT, averageWordLength FLOAT(10,2), duplicateAmount INT);");
    }

    @Override
    public void write(DataReport dataReport) {
        dataReport.getEntries().forEach(dataReportEntry -> {
            jdbcTemplate.update("INSERT INTO reports " +
                    "(longestName, shortestName, lineLength, averageWordLength, duplicateAmount)" +
                    "VALUES(?, ?, ?, ?, ?)",
                    dataReportEntry.getLongestWord(), dataReportEntry.getShortestWord(),
                    dataReportEntry.getLineLength(), dataReportEntry.getAverageWordLength(),
                    dataReportEntry.getDuplicateAmount());
        });
    }
}
