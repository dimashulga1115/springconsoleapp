package com.project.springconsoleapp;

import com.project.springconsoleapp.analyzer.DataAnalyzer;
import com.project.springconsoleapp.analyzer.DataItem;
import com.project.springconsoleapp.config.ModuleConfiguration;
import com.project.springconsoleapp.reader.DataReader;
import com.project.springconsoleapp.writer.DataWriter;

public class Module {
    private ModuleConfiguration config;

    public Module(ModuleConfiguration configuration) {
        this.config = configuration;
    }

    public void run(){
        DataReader reader = config.getDataReader();
        DataAnalyzer analyzer = config.getDataAnalyzer();
        DataWriter writer = config.getDataWriter();
        writer.write(analyzer.getReport(reader.read()));
    }

    public void run(DataItem dataItem){
        DataAnalyzer analyzer = config.getDataAnalyzer();
        DataWriter writer = config.getDataWriter();
        writer.write(analyzer.getReport(dataItem));
    }

}
