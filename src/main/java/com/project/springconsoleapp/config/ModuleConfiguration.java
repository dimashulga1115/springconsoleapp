package com.project.springconsoleapp.config;

import com.project.springconsoleapp.analyzer.DataAnalyzer;
import com.project.springconsoleapp.reader.DataReader;
import com.project.springconsoleapp.writer.DataWriter;

public interface ModuleConfiguration {
    DataReader getDataReader();
    DataAnalyzer getDataAnalyzer();
    DataWriter getDataWriter();
}
