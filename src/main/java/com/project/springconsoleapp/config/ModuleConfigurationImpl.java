package com.project.springconsoleapp.config;

import com.project.springconsoleapp.analyzer.DataAnalyzer;
import com.project.springconsoleapp.reader.DataReader;
import com.project.springconsoleapp.writer.DataWriter;
import org.springframework.context.annotation.Bean;

public class ModuleConfigurationImpl implements ModuleConfiguration{
    DataReader dataReader;
    DataAnalyzer dataAnalyzer;
    DataWriter dataWriter;

    private ModuleConfigurationImpl(){}

    private ModuleConfigurationImpl(ModuleConfigurationImpl configuration) {
        this.dataReader = configuration.dataReader;
        this.dataAnalyzer = configuration.dataAnalyzer;
        this.dataWriter = configuration.dataWriter;
    }

    @Override
    public DataReader getDataReader() {
        return dataReader;
    }

    @Override
    public DataAnalyzer getDataAnalyzer() {
        return dataAnalyzer;
    }

    @Override
    public DataWriter getDataWriter() {
        return dataWriter;
    }

    public static Builder getBuilder(){
        return new ModuleConfigurationImpl().new Builder();
    }

    public class Builder{
        private Builder(){}

        public Builder setDataReader(DataReader reader){
            ModuleConfigurationImpl.this.dataReader = reader;
            return this;
        }

        public Builder setDataAnalyzer(DataAnalyzer analyzer){
            ModuleConfigurationImpl.this.dataAnalyzer = analyzer;
            return this;
        }

        public Builder setDataWriter(DataWriter writer){
            ModuleConfigurationImpl.this.dataWriter = writer;
            return this;
        }

        public ModuleConfigurationImpl build(){
            return new ModuleConfigurationImpl(ModuleConfigurationImpl.this);
        }
    }
}
