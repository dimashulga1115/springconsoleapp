package com.project.springconsoleapp.analyzer;

public class DataItem {
    private String data;

    public DataItem(String data) {
        this.data = data;
    }

    public String getData(){
        return data;
    }
}
