package com.project.springconsoleapp.analyzer;

public class DataAnalyzer {
    DataReport report;

    public DataAnalyzer(){}

    public DataReport getReport(DataItem dataItem) {
        report = new DataReport();
        String text = dataItem.getData();
        String[] lines = text.split("\n");
        for(String line : lines){
            report.addLine(line);
        }
        report.addLine(text.replaceAll("\n", " "));
        return report;
    }
}
