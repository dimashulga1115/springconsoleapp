package com.project.springconsoleapp.analyzer;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

@Component
public class DataReport {

    List<DataReportEntry> entries;

    public DataReport() {
        entries = new ArrayList<>();
    }

    public void addLine(String line){
        entries.add(new DataReportEntry(line));
    }

    public List<DataReportEntry> getEntries() {
        return entries;
    }

    public class DataReportEntry{
        String longestWord;
        String shortestWord;
        int lineLength;
        double averageWordLength;
        int duplicateAmount;

        public String getLongestWord() {
            return longestWord;
        }

        public String getShortestWord() {
            return shortestWord;
        }

        public int getLineLength() {
            return lineLength;
        }

        public double getAverageWordLength() {
            return averageWordLength;
        }

        public int getDuplicateAmount() {
            return duplicateAmount;
        }

        public DataReportEntry(String line){
            List<String> list = Arrays.asList(line.split(" "));
            longestWord = list.stream().max(Comparator.comparing(String::length)).get();
            shortestWord = list.stream().min(Comparator.comparing(String::length)).get();
            lineLength = line.length();
            averageWordLength = list.stream().mapToInt(s -> s.length()).summaryStatistics().getAverage();
            duplicateAmount = list.size() - (int) list.stream().distinct().count();
        }

        @Override
        public String toString() {
            return "DataReportEntry{" +
                    "longestWord='" + longestWord + '\'' +
                    ", shortestWord='" + shortestWord + '\'' +
                    ", lineLength=" + lineLength +
                    ", averageWordLength=" + averageWordLength +
                    ", duplicateAmount=" + duplicateAmount +
                    '}';
        }
    }
}
