package com.project.springconsoleapp;

import com.project.springconsoleapp.analyzer.DataAnalyzer;
import com.project.springconsoleapp.analyzer.DataItem;
import com.project.springconsoleapp.config.ModuleConfigurationImpl;
import com.project.springconsoleapp.reader.FileReader;
import com.project.springconsoleapp.writer.ConsoleWriter;
import com.project.springconsoleapp.writer.sql.SqlWriter;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        ModuleConfigurationImpl config = ModuleConfigurationImpl.getBuilder()
                .setDataAnalyzer(new DataAnalyzer())
//                .setDataWriter(new ConsoleWriter())
                .setDataWriter(new SqlWriter())
                .build();

        DataItem dataItem = new FileReader("src/main/resources/files/file1.txt").read();
        Module module = new Module(config);
        module.run(dataItem);
    }
}
