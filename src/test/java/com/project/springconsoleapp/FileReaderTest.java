package com.project.springconsoleapp;

import com.project.springconsoleapp.reader.FileReader;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.*;

public class FileReaderTest {
    @Test
    public void testFileReader()
            throws IOException {
        FileReader reader = new FileReader("src/test/resources/test.txt");

        String read = reader.read().getData();
        assertEquals("qwerty a\npe pe pe pe pe pe t longest\n", read);

        assertThrows(IOException.class, () -> {new FileReader("wrongpath"); });
    }
}
