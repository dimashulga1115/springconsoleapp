package com.project.springconsoleapp;

import com.project.springconsoleapp.analyzer.DataAnalyzer;
import com.project.springconsoleapp.analyzer.DataItem;
import com.project.springconsoleapp.analyzer.DataReport;
import org.junit.Test;

import static org.junit.Assert.*;

public class DataAnalyzerTest {
    @Test
    public void testDataAnalyzer(){
        DataItem dataItem = new DataItem("qwerty a\npe pe pe pe pe pe t longest\n");
        DataReport dataAnalyzer = new DataAnalyzer().getReport(dataItem);

        assertEquals(dataAnalyzer.getEntries().get(0).getLongestWord(), "qwerty");
        assertEquals(dataAnalyzer.getEntries().get(0).getShortestWord(), "a");
        assertEquals(dataAnalyzer.getEntries().get(0).getLineLength(), 8);
        assertEquals(dataAnalyzer.getEntries().get(0).getAverageWordLength(), 3.5f, 0.01f);
        assertEquals(dataAnalyzer.getEntries().get(0).getDuplicateAmount(), 0);

        assertEquals(dataAnalyzer.getEntries().get(1).getLongestWord(), "longest");
        assertEquals(dataAnalyzer.getEntries().get(1).getShortestWord(), "t");
        assertEquals(dataAnalyzer.getEntries().get(1).getLineLength(), 27);
        assertEquals(dataAnalyzer.getEntries().get(1).getAverageWordLength(), 2.5f, 0.01f);
        assertEquals(dataAnalyzer.getEntries().get(1).getDuplicateAmount(), 5);

        assertEquals(dataAnalyzer.getEntries().get(2).getLongestWord(), "longest");
        assertEquals(dataAnalyzer.getEntries().get(2).getShortestWord(), "a");
        assertEquals(dataAnalyzer.getEntries().get(2).getLineLength(), 37);
        assertEquals(dataAnalyzer.getEntries().get(2).getAverageWordLength(), 2.7f, 0.01f);
        assertEquals(dataAnalyzer.getEntries().get(2).getDuplicateAmount(), 5);

    }
}
