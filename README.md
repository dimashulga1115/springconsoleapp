# SpringConsoleApp

## To run the app

Just run `mvn spring-boot:run`

To run unit tests run `mvn test`

To build jar file run `mvn clean package`

## Preparations

This app needs to have mysql database working on your computer.
To be able to use it you need to:
- install and run mysql database. On linux do
 
      sudo apt install mysql; sudo service mysql start;
      
- create user in a database and grant him permissions. On linux 
     
      sudo mysql; create user 'testuser'@'localhost' identified by 'testpass'; 
      grant all privileges on test.* to 'testuser'@'localhost';

## Technical requirements

Implement console application (mandatory)
  - [x] Read txt file and split it by lines
  - [x] Calculate statistics for each line and whole file:\
        - longest word (symbols between 2 spaces),\
        - shortest word,\
        - line length,\
        - average word length,\
        - duplication of the words.
  - [x] Store line and file statistic into DB (with JDBC).

Optional tasks
  - [x] Add unit tests
  - [x] Preferred to use maven as the build tool.
  - [x] The project should be placed in GitHub(or other shared repository) 
  - [x] and be buildable.
  - [x] Please add ReadMe file with information and command for run the app.